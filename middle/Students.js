const { Students } = require('../models/Students');

class StudentsMiddle {

    // 添加新学生
    static async addNewStudent(val) {

        // const hasStudent = await Students.findOne({
        //     where: {
        //         connections: val.connections
        //     }
        // })

        // if(hasStudent) throw new Error('该手机号已经注册过用户!')

        await Students.create(val)
    }

    // 查询学生列表
    static async findAllStudents() {
        const studentsList = await Students.findAll()
        return studentsList;
    }
}

module.exports = {
    StudentsMiddle
}
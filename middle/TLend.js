/**
 * 借阅
 */

const { sequelize } = require('../core/db');
const { Sequelize, DataTypes, Model} = require('sequelize');

class TLend extends Model {

};

// 初始化Admin模型
TLend.init({
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },

    name: {
        type: DataTypes.STRING,
    },

    level: {
        type: DataTypes.ENUM,
        values: ['system', 'server', 'user'],
        allowNull: false
    },

    address: {
        type: DataTypes.STRING,
        allowNull: true
    },

    isCheck: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    }
},{
    sequelize,
    tableName: 'tlend'  //自定义表名
})

module.exports = {
    TLend
}
const { User } = require('../models/user')

class UserMiddle {
    constructor(name){
        this.name = name;
    }

    // 获取所有用户
    static async getAllUser() {
        console.log('调用查询方法！')
        const userList = await User.findOne({
            where: {
                nickname: 'tom'
            }
        })
        console.log('search-action-success')
        if(!userList) {
            throw new Error('该用户不存在！')
        }

        return userList
    }
}

module.exports = {
    UserMiddle
}
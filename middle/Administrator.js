/**
 * 处理管理员的中间件
 */

 const { Administrator } = require('../models/Administrator');

 class AdministratorMiddle {

    // 添加管理员
    static async addAdministrator(val){

        // const hasAdmin = await Administrator.findOne({
        //     where:{
        //         nickname: val.nickname,
        //         password: val.password
        //     }
        // })
        // if(hasAdmin) throw new Error('管理员已存在')

        await Administrator.create(val)

    }

    // 查询管理员
    static async findAdministrator(val) {
        const administrator = await Administrator.findOne({
            where:{
                nickname: val.nickname,
                password: val.password
            }
        })

        return administrator;
    }

    // 查询管理员列表
    static async findAdministratorAll() {
        
        const Ad = await Administrator.findAll()
        
        return Ad;
    }

    // 删除管理员
    static async deleteAdministrator(aid) {
        const deleteAdmin = await Administrator.findOne({
            where: {
                aid
            }
        })

        if(!deleteAdmin) {
            throw new Error('不存在该条记录！删除操作错误！')
        }

        deleteAdmin.destroy();
    }
 }

 module.exports = {
    AdministratorMiddle
 }
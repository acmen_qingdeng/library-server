/**
 * 处理归还书表
 */

 const { HLend } = require('../models/HLend');

 class HLendMiddle {

    // 添加已归还书籍
    static async addHLend(val) {
        HLend.create(val)
    }

    static async deleteItems(hid) {
        const deleteId = await HLend.findOne({
            where: {
                hid
            }
        });
        if(!deleteId) {
            throw new Error('该条记录不存在，删除操作错误！')
        }
        deleteId.destroy();
    }
 }

 module.exports = {
     HLendMiddle
 }
const { sequelize } = require('../core/db');
const { Sequelize, DateTypes, Model, Op } = require("sequelize");
const { Admin } = require('../models/Admin');

const Mock = require('mockjs');

const AdminData = Mock.mock({
    'data|30': [
        {
            'id|+1': 1000,
        
            'name': '@cfirst'+'@clast',
        
            'level|1': ['system', 'server', 'user'] ,
        
            'address': '@county(true)',
        
            'isCheck|1-3': true
        }
    ]
})


async function insertGroup(arr) {
    console.log('同步模型');
    try {
        await  Admin.sync();
    } catch(err) {
        console.log(err)
    }
    if(arr instanceof Array){
        const res = await Admin.bulkCreate(arr);
        console.log('操作结果', res)
    }else{
        console.log('批量插入--要求一个数组作为参数')
    }
}

sequelize.authenticate()
.then( res => {
    console.log('connection has been online');
    const dataReady = AdminData.data;
    console.log('dataReady', dataReady);
    // 入库
    insertGroup(dataReady);

})
.catch( err => {
    console.log('connection has been outline')
})

class Man {
    constructor(name, age, color, hobby) {
        this.name = name,
        this.age = age,
        this.color = color,
        this.hobby = hobby || 'cooking'
    }

    outPut(val) {
        console.log(`${this.name}的属性${val}是${this[val]}`)
    }

    updateAge(newAge) {
        this.age = newAge;
        console.log(`${this.name}的年龄已经更改为${this.age}`)
    }


    static outPutStatic() {
        console.log(JSON.stringify(this))
    }
}

const zhangsan = new Man('zhangsan', 18, 'red', 'table');

zhangsan.outPut('color');

zhangsan.updateAge(24);

// zhangsan.outPutStatic();     //父类的静态方法，不会被实例继承

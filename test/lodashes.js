const { dropRightWhile, differenceWith, isEqual, pullAllBy,
        nth, pull, remove, sortedUniq, find
} = require('lodash');

const LIST = [
    { id: 1, name: 'zhangsan1', isCheck: true},
    { id: 2, name: 'zhangsan2', isCheck: false},
    { id: 3, name: 'zhangsan3', isCheck: true},
    { id: 4, name: 'zhangsan4', isCheck: true},
    { id: 5, name: 'zhangsan5', isCheck: false},
    { id: 6, name: 'zhangsan6', isCheck: true},
    { id: 7, name: 'zhangsan7', isCheck: false},
    { id: 8, name: 'zhangsan8', isCheck: false},
    { id: 9, name: 'zhangsan9', isCheck: false},
]


const users = [
    { 'user': 'tom', 'age': 36, 'active': true},
    { 'user': 'tomson', 'age': 36, 'active': true},
    { 'user': 'jack', 'age': 36, 'active': false},
    { 'user': 'tomy', 'age': 36, 'active': true},
]

const loser = find(users, (o)=> {
    return !o.active
})

console.log('loser', loser)
// const Arr = [1,2,2,3,4,10,8]     //demo有问题
// const sortArr = sortedUniq(Arr);
// console.log('sortArr', sortArr);

// const lastList = remove(LIST,(n) => {
//     return n.isCheck
// })
// console.log('LIST', LIST);
// console.log('lastList', lastList)

// const ARRAY =  ['1', '2', '3', '4', '5', '6'];
// pull(ARRAY,'3')
// console.log('pull', ARRAY)

// console.log('nth', nth(ARRAY,1))

// const dropRightList = dropRightWhile(LIST,{'isCheck': false});
// console.log("dropRIghtLIst", dropRightList);

// const newList = LIST.slice()
// console.log('newList', newList)
// pullAllBy(newList, (item)=> {    // 使用方法有误
//     console.log('item', item)
//     if(item.isCheck){
//         return item
//     } 
// })
// console.log('handleNewList', newList)



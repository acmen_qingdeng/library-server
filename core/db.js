const { Sequelize } = require('sequelize');

const {
    dbName,
    host,
    port,
    user,
    password
} = require('../config/config').database;

const sequelize = new Sequelize(dbName, user, password, {
    dialect: 'mysql',
    host,
    port
});

// try {
//     await sequelize.authenticate();
//     console.log('Connection has benn established success!');
// } catch (error) {
//     console.log('Connection has benn established failed!');
// }

module.exports = {
    sequelize
}


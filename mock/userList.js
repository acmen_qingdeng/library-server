const Mock = require('mockjs');

const userList = Mock.mock({
    "array|1-10": [
        {
            'id|+1': 10002,
            'name': '@cfirst' + '@clast' ,
            'address': '@city'
        }
      ]
})

const syncList = userList;
// console.log(userList);
module.exports = {
    userList,
    syncList
}
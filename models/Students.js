/**
 * 学生表
 * 借书证号，密码，姓名，性别，出生时间，专业，借书量，照片，备注，联系方式
 */
const { sequelize } = require('../core/db');
const { Sequelize, DataTypes, Model } = require('sequelize');

class Students extends Model {

};

Students.init({
    lendId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    password: {
        type: DataTypes.STRING    
    },
    uName: DataTypes.STRING,
    uSex: {
        type: DataTypes.INTEGER,
        defaultValue: 0,    //默认0-女；1-男；2-其他；
    },
    birthday: DataTypes.STRING,
    expertise: DataTypes.STRING, // 专业
    lendCounts: DataTypes.INTEGER,  // 借书数量
    headImg: DataTypes.STRING,
    uMsg: DataTypes.STRING,
    connections: DataTypes.STRING
}, {
    sequelize,
    tableName: 'students'
})
// Students.sync() 
module.exports = {
    Students
}
/**
 * 借阅表
 * 借书证号，ISBN，图书id，借书时间，应还时间
 */
const { sequelize } = require('../core/db');
const { Sequelize, DataTypes, Model } = require('sequelize');
const { TBook } = require('./Tbook');

class TLend extends Model {

};

TLend.init({
    bookId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    TBook_ISBN: {
        type: DataTypes.INTEGER,
        references: {
            // 这是对另一个模型的参考
            model: TBook,
      
            // 这是引用模型的列名
            key: 'ISBN',

            // 使用 PostgreSQL,可以通过 Deferrable 类型声明何时检查外键约束.
            deferrable: Deferrable.INITIALLY_IMMEDIATE
            // 参数:
            // - `Deferrable.INITIALLY_IMMEDIATE` - 立即检查外键约束
            // - `Deferrable.INITIALLY_DEFERRED` - 将所有外键约束检查推迟到事务结束
            // - `Deferrable.NOT` - 完全不推迟检查(默认) - 这将不允许你动态更改事务中的规则
          }
    },
    TBook_bid: {
        type: DataTypes.STRING,
        references: {
            model: TBook,
            key: 'bid',
            deferrable: Deferrable.INITIALLY_IMMEDIATE
          }
    },
    lendTime: DataTypes.STRING,
    backTime: DataTypes.STRING
}, {
    sequelize,
    tableName: 'tlend'
})

module.exports = {
    TLend
}
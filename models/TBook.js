/**
 * 图书表
 * ISBN，书名，作者（译者），出版社，出版年月，价格，复本量，库存量，分类号，内容摘要，封面照片
 */
const { sequelize } = require('../core/db');
const { Sequelize, DataTypes, Model } = require('sequelize');

class TBook extends Model {

};

TBook.init({
    bid: {
        type: DataTypes.STRING,
        autoIncrement: true
    },
    ISBN: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    bookName: DataTypes.STRING,
    bookAuthor: DataTypes.STRING,
    press: DataTypes.STRING, // 出版社
    publishData: DataTypes.TIME,
    bookPrice: DataTypes.STRING,
    bookCopy: DataTypes.INTEGER,
    bookStore: DataTypes.INTEGER,
    bookClass: DataTypes.INTEGER,
    bookContent: DataTypes.STRING,
    bookImg: DataTypes.STRING
},{
    sequelize,
    tableName: 'tbook'
})


module.exports = {
    TBook
}
const moment = require('moment');
const bcrypt = require('bcryptjs');
const { sequelize } = require('../core/db');
const { Sequelize, DataTypes, Model } = require('sequelize');

// 使用Model模型扩展定义模型
class User extends Model {

};

User.init({
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    // 用户昵称
    nickname: DataTypes.STRING,
    // 用户地址
    phone: DataTypes.INTEGER,
    // password: {
    //     type: DataTypes.STRING,
    //     set(v) {
    //         // 加密
    //         const salt = bcrypt.genSalt(10);
    //         // 生成加密密码
    //         const psw = bcrypt.hashSync(v, salt)
    //         this.setDataValue('password', psw)
    //     }
    // },
    created_at: {
        type: DataTypes.DATE,
        get() {
            return moment(this.getDataValue('created_at')).format('YYYY-MM-DD')
        }
    }

},{
    sequelize,
    tableName: 'user'  //自定义表名
})


module.exports = {
    User
}
const { sequelize } = require('../core/db');
const { Sequelize, DataTypes, Model} = require('sequelize');

class Admin extends Model {

};

// 初始化Admin模型
Admin.init({
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },

    name: {
        type: DataTypes.STRING,
    },

    level: {
        type: DataTypes.ENUM,
        values: ['system', 'server', 'user'],
        allowNull: false
    },

    address: {
        type: DataTypes.STRING,
        allowNull: true
    },

    isCheck: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    }
},{
    sequelize,
    tableName: 'admin'  //自定义表名
})

module.exports = {
    Admin
}
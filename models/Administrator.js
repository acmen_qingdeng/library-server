/**
 * 管理员表
 * 角色名，密码，备注
 */
const { sequelize } = require('../core/db');
const { Sequelize, DateTypes, Model, DataTypes } = require('sequelize');

class Administrator extends Model {

};

Administrator.init({
    aid: {
        type: DataTypes.INTEGER(5),
        primaryKey: true,
        autoIncrement: true
    },
    nickname: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    password: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    adminMsg: {
        type: DataTypes.STRING,
        allowNull: true
    }
}, {
    sequelize,
    tableName: 'administrator'  //自定义表名
})

// Administrator.sync();   // 临时解决办法

module.exports = {
    Administrator
}
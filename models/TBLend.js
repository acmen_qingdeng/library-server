/**
 * 借阅表
 * 借书证号，ISBN，图书id，借书时间，应还时间
 */
const { sequelize } = require('../core/db');
const { Sequelize, DataTypes, Model } = require('sequelize');
const { TBook } = require('./Tbook');

class TBLend extends Model {

};

TBLend.init({
    bookId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    ISBN: {
        type: DataTypes.INTEGER,
    },
   isOout: DataTypes.BOOLEAN,
}, {
    sequelize,
    tableName: 'tblend'
})

module.exports = {
    TBLend
}
# libraryServer

#### 介绍
node 、koa2、mysql、sequelize构建第一个全栈项目，萌新的怒吼！！！

#### 软件架构
软件架构说明


#### 安装教程

1.  下载项目 git clone xxxx pull项目，然后进入项目目录
2.  使用`npm install`安装依赖
3.  使用`npm run start`运行项目

#### 使用说明

1. config目录

   包含数据库的配置信息，本项目使用的mysql数据库，使用本项目之前，请确认数据库的配置信息；

2. modles目录

   定义数据库model文件，本项目使用sequelizeJS，通过model来管理数据库信息；在本目录下定义了项目中要使用的6张表；

3. middle目录

   处理业务逻辑的文件，主要定义操作数据库的方法，由于目前的操作很简单，只是自己的测试用，所以全部方法直接定义在这里，后面会继续完善。

4. routes目录

   定义服务器的接口

5. mock目录

   定义部分虚拟数据，为了方便操作数据库，进行一些代码的测试工作，我试用mockJS生成了一些假数据导入数据库。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

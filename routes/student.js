const router = require('koa-router')()

const { StudentsMiddle } = require('../middle/Students')

router.prefix('/student')

router.get('/bar', function (ctx, next) {
    ctx.body = 'this is a users/bar response'
  })

router.get('/studentsAll', async (ctx, next) => {
    const stu = await StudentsMiddle.findAllStudents()
    
    let res;
    if(stu) {
        res = {
            code: '2001',
            msg: 'get msg successed',
            data: stu
        }
    } else {
        res = {
            code: '5001',
            msg: 'get msg failed',
            data: stu
        }
    }
  ctx.body = res
})

router.get('/addStudent', async (ctx, next) => {
    const data = ctx.request.query;

    await StudentsMiddle.addNewStudent(data);

    ctx.body =  {
        code: '2001',
        msg: 'add msg successed'
    }
})

module.exports = router

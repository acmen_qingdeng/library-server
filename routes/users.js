const router = require('koa-router')()

const user = require('../mock/userList');
// const user = require('../models/user')
const { UserMiddle } = require('../middle/user')
// const { Admin } = require('../models/Admin'); 

router.prefix('/users')

router.get('/', function (ctx, next) {
  ctx.body = 'this is a users response!'
})

router.get('/bar', function (ctx, next) {
  ctx.body = 'this is a users/bar response'
})

router.get('/userList',async (ctx) =>{
  console.log('ctx', ctx);
  console.log('ctx.request.body', ctx.request.body);
  console.log('ctx.request.query', ctx.request.query);
  console.log('ctx.params', ctx.params);
  console.log('ctx.request.header', ctx.request.header);

  const userList = await UserMiddle.getAllUser();
  response = {
    status: 200,
    message: '请求成功',
    data: userList
  }
  ctx.body = response;
})


module.exports = router

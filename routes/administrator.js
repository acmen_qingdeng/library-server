const router = require('koa-router')()
const { AdministratorMiddle } = require('../middle/Administrator');

router.prefix('/admin');


router.get('/addAdmin', async (ctx, next) => {
    const query = ctx.request.query;
    const body = ctx.request.body;

    const admin = await AdministratorMiddle.addAdministrator(query)

    ctx.body = {
        isAdd: true
    }
})

router.get('/findAdmin', async (ctx, next) => {
    const query = ctx.request.query;
    const admin = await AdministratorMiddle.findAdministrator(query)

    ctx.body = {
        admin: admin
    }
})

router.get('/adminAll', async (ctx, next) => {

    const adminAll = await AdministratorMiddle.findAdministratorAll();

    let res = {
        code: '2001',
        msg: 'get msg successed',
        data: adminAll
    }
    ctx.body = res;
})

router.delete('/deleteAdmin', async (ctx) => {
    await AdministratorMiddle.deleteAdministrator();
    let res = {
        code: '2001',
        msg: 'get msg successed',
        data: {
            action: true
        }
    }
    ctx.body = res;
})


module.exports = router